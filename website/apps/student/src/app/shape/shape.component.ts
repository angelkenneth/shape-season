import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@shape-season/document/services';
import { RouteName } from '@shape-season/shared/enums';

@Component({
  selector: 'ss-shape',
  templateUrl: './shape.component.html',
  styleUrls: ['./shape.component.scss'],
})
export class ShapeComponent {
  constructor(
    private readonly user: UserService,
    private readonly router: Router,
  ) {
  }

  async signOut() {
    await this.user.signOut();
    await this.router.navigate(['/', RouteName.signIn]);
  }
}
