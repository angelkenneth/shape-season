from decimal import Decimal


def area_of_triangle(base: Decimal, height: Decimal) -> Decimal:
    return base * height / 2


def area_of_rectangle(base: Decimal, height: Decimal) -> Decimal:
    return base * height


def area_of_square(length: Decimal) -> Decimal:
    return length * length


def area_of_diamond(base: Decimal, height: Decimal) -> Decimal:
    return base * height / 2


def perimeter_of_isosceles_triangle(base: Decimal, height: Decimal) -> Decimal:
    return base + (base ** 2 + 4 * height ** 2).sqrt()


def perimeter_of_rectangle(base: Decimal, height: Decimal) -> Decimal:
    return (base + height) * 2


def perimeter_of_square(length: Decimal) -> Decimal:
    return length * 4


def perimeter_of_diamond(base: Decimal, height: Decimal) -> Decimal:
    return (base ** 2 + height ** 2).sqrt() * 4
