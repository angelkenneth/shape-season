from rest_framework.status import HTTP_200_OK


class TestPing:
    def test_ping(self, anonymous_api):
        response = anonymous_api.post('/api/shapes/ping')
        assert response.status_code == HTTP_200_OK
        assert response.data == {'hello': 'world'}
