interface ErrorDetail {
  code: string;
  message?: string;
}

export type ErrorDetails = ErrorDetail[];
export type ErrorDetailMap = Record<string, ErrorDetails>;
export type ErrorCodeMap = Record<string, string>;

export function errorDetailToCodeMap(details: ErrorDetails | undefined): ErrorCodeMap {
  if (details) {
    return details.reduce((m, d) => ({ ...m, [d.code]: d.message || d.code }), {} as ErrorCodeMap);
  } else {
    return {}
  }
}
