# ShapeSeason

1. Code Repository: https://gitlab.com/angelkenneth/shape-season

## Instructions

This project was developed using Ubuntu 20.04,
    but may also run on Windows 10 Pro.

1. Install Docker, Docker Compose and NodeJS on your system
1. Run both Django and Angular:
    1. To run the Django Backend:
    
        ```bash
        npm run migrate  # you only need to run this once
        npm run start
        ```
    
    1. To run the Angular Frontend:
    
        ```bash
        cd website 
        npm run local:install  # you only need to run this once
        npm run start  
        ```

1. Access website: http://localhost:4200
1. (Optional) To run test: `npm run test`

## Requirements' Code Locations

1. User Registration and Login:  [apps/user/api/viewsets.py](apps/user/api/viewsets.py)
1. Shape CRUD:  [apps/shape/api/viewsets.py](apps/shape/api/viewsets.py)
1. Area and Perimeter calculator:  [apps/shape/api/serializers.py](apps/shape/api/serializers.py)

## Requirements' GIFs

1. User Registration

    ![User Registration](gifs/user-registration.gif)

1. User Login

    ![User Login](gifs/user-login.gif)
    
1. Read Shape; calculated Area and Perimeter

    ![Read Shape](gifs/shape-read.gif)

1. Create Shape

    ![Create Shape](gifs/shape-create.gif)

1. Update Shape

    ![Update Shape](gifs/shape-update.gif)
    
1. Delete Shape

    ![Delete Shape](gifs/shape-delete.gif)
