import pytest
from rest_framework import status


@pytest.mark.django_db
class TestUserSignOut:
    def test_sign_out(self, user_api):
        logout_response = user_api.delete('/api/users/sign-out')
        assert logout_response.status_code == status.HTTP_204_NO_CONTENT

    def test_anonymous_sign_out(self, anonymous_api):
        logout_response = anonymous_api.delete('/api/users/sign-out')
        assert logout_response.status_code == status.HTTP_204_NO_CONTENT

