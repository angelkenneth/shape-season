from rest_framework.exceptions import ValidationError, ErrorDetail


def field_error(field_name, code, message=None):
    return ValidationError({field_name: [ErrorDetail(message or code, code=code)]})
