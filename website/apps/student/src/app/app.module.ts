import { HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ShapeService, UserService } from '@shape-season/document/services';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { XRequestedWithInterceptor } from './http-interceptor';

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({ cookieName: 'csrftoken', headerName: 'X-CSRFToken' }),
  ],
  providers: [
    UserService,
    ShapeService,
    { provide: HTTP_INTERCEPTORS, useClass: XRequestedWithInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
