import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@shape-season/document/services';
import { ErrorDetailMap, errorDetailToCodeMap } from '@shape-season/document/shapes';
import { RouteName } from '@shape-season/shared/enums';

@Component({
  selector: 'ss-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  constructor(
    private readonly fb: FormBuilder,
    private readonly user: UserService,
    private readonly router: Router,
  ) {
  }

  public readonly RouteName = RouteName;
  public readonly form = this.fb.group({
    email: ['', Validators.required],
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    password: ['', Validators.required],
  });

  async submit() {
    await this.user.signOut();
    await this.user.register(this.form.value)
      .then(() => this.router.navigate(['/', RouteName.shapes]))
      .catch((error: HttpErrorResponse) => {
        const validationError = error.error as ErrorDetailMap;

        this.form.get('email').setErrors(errorDetailToCodeMap(validationError.email));
        this.form.get('first_name').setErrors(errorDetailToCodeMap(validationError.first_name));
        this.form.get('last_name').setErrors(errorDetailToCodeMap(validationError.last_name));
        this.form.get('password').setErrors(errorDetailToCodeMap(validationError.password));
      });
  }
}
