import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BehaviorSubject } from 'rxjs';
import { ShapeCreateComponent } from './shape-create/shape-create.component';
import { ShapeListComponent } from './shape-list/shape-list.component';

import { ShapeRoutingModule } from './shape-routing.module';
import { ShapeComponent } from './shape.component';
import { REFRESH_LIST } from './shape.tokens';
import { ShapeItemComponent } from './shape-item/shape-item.component';
import { ShapeUpdateComponent } from './shape-update/shape-update.component';


@NgModule({
  declarations: [
    ShapeComponent,
    ShapeCreateComponent,
    ShapeItemComponent,
    ShapeListComponent,
    ShapeUpdateComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    ShapeRoutingModule,
  ],
  providers: [
    { provide: REFRESH_LIST, useValue: new BehaviorSubject(null) },
  ],
})
export class ShapeModule {
}
