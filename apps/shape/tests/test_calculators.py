from decimal import Decimal

import pytest

from .. import calculators


def test_area_of_triangle():
    assert calculators.area_of_triangle(Decimal('3'), Decimal('4')) == Decimal('6')


def test_area_of_rectangle():
    assert calculators.area_of_rectangle(Decimal('3'), Decimal('4')) == Decimal('12')


def test_area_of_square():
    assert calculators.area_of_square(Decimal('3')) == Decimal('9')


def test_area_of_diamond():
    assert calculators.area_of_diamond(Decimal('3'), Decimal('4')) == Decimal('6')


def test_perimeter_of_rectangle():
    assert calculators.perimeter_of_rectangle(Decimal('3'), Decimal('4')) == Decimal('14')


def test_perimeter_of_square():
    assert calculators.perimeter_of_square(Decimal('3')) == Decimal('12')


class TestTriablePerimeter:
    def test_isosceles_triangle(self):
        assert calculators.perimeter_of_isosceles_triangle(Decimal('6'), Decimal('4')) == Decimal('16')

    @pytest.mark.skip("Not anticipated for now")
    def test_right_triangle(self):
        assert calculators.perimeter_of_isosceles_triangle(Decimal('3'), Decimal('4')) == Decimal('12')

    @pytest.mark.skip("Not anticipated for now")
    def test_obtuse_triangle(self):
        assert calculators.perimeter_of_isosceles_triangle(Decimal('6'), Decimal('4')) == Decimal('16')


class TestDiamondPerimeter:
    def test_pythagorian_triple(self):
        assert calculators.perimeter_of_diamond(Decimal('3'), Decimal('4')) == Decimal('20')
