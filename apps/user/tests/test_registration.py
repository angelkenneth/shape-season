import pytest
from django.contrib.auth.models import User
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_201_CREATED


@pytest.mark.django_db
class TestUserRegistration:
    def test_requried_fields(self, anonymous_api):
        response = anonymous_api.post('/api/users')
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['first_name'][0]['code'] == 'required'
        assert response.data['last_name'][0]['code'] == 'required'
        assert response.data['email'][0]['code'] == 'required'
        assert response.data['password'][0]['code'] == 'required'

    def test_normal_user(self, anonymous_api, faker):
        password = faker.password()
        response = anonymous_api.post('/api/users', {
            'email': faker.email(),
            'first_name': faker.first_name(),
            'last_name': faker.last_name(),
            'password': password,
        })
        assert response.status_code == HTTP_201_CREATED

        user = User.objects.get(pk=response.data['pk'])
        assert user.check_password(password)

    def test_unique_email(self, anonymous_api, faker):
        email = faker.email()
        User.objects.create_user(email, email, 'abc123')
        response = anonymous_api.post('/api/users', {
            'email': email,
            'first_name': faker.first_name(),
            'last_name': faker.last_name(),
            'password': faker.password(),
        })
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['email'][0]['code'] == 'unique'
