from rest_framework import serializers, fields

from apps.shape import calculators
from apps.shape.enums.shape_type import ShapeType
from apps.shape.models import Shape


class ShapeSerializer(serializers.ModelSerializer):
    pk = fields.CharField(read_only=True)

    class Meta:
        model = Shape
        fields = ('pk', 'id',
                  'shape_type',
                  'base',
                  'height',
                  )

    def to_internal_value(self, data):
        validated_data = super().to_internal_value(data)
        shape_type = validated_data.get('shape_type')
        base = validated_data.get('base')
        height = validated_data.get('height')

        if self.instance:
            shape_type = self.instance.shape_type if shape_type is None else shape_type
            base = self.instance.base if base is None else base
            height = self.instance.height if height is None else height

        if shape_type == ShapeType.Triangle:
            area = calculators.area_of_triangle(base, height)
            perimeter = calculators.perimeter_of_isosceles_triangle(base, height)
        elif shape_type == ShapeType.Rectangle:
            area = calculators.area_of_rectangle(base, height)
            perimeter = calculators.perimeter_of_rectangle(base, height)
        elif shape_type == ShapeType.Square:
            area = calculators.area_of_square(base)
            perimeter = calculators.perimeter_of_square(base)
        elif shape_type == ShapeType.Diamond:
            area = calculators.area_of_diamond(base, height)
            perimeter = calculators.perimeter_of_diamond(base, height)

        # noinspection PyUnboundLocalVariable
        return {
            **validated_data,
            'area': area,
            'perimeter': perimeter,
        }

    def to_representation(self, instance):
        return ShapeReadSerializer(instance).data


class ShapeReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shape
        fields = (
            'pk', 'id',
            'created',
            'shape_type',
            'base',
            'height',
            'area',
            'perimeter',
        )
