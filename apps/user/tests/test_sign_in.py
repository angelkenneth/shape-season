import pytest
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST


@pytest.mark.django_db
class TestUserSignIn:
    def test_login_user_via_email(self, anonymous_api, user, user_password):
        response = anonymous_api.post("/api/users/sign-in", {'email': user.email, 'password': user_password})
        assert response.status_code == HTTP_200_OK

    def test_login_user_via_case_insensitive_email(self, anonymous_api, user, user_password):
        email_parts = list(user.email.partition('@'))
        email_parts[2] = email_parts[2].upper()
        email = "".join(email_parts)

        response = anonymous_api.post("/api/users/sign-in", {'email': email, 'password': user_password})
        assert response.status_code == HTTP_200_OK

    def test_not_allow_empty_email_and_password(self, anonymous_api):
        response = anonymous_api.post("/api/users/sign-in")
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['email'][0]['code'] == 'required'
        assert response.data['password'][0]['code'] == 'required'

    def test_not_allow_empty_email(self, anonymous_api):
        response = anonymous_api.post("/api/users/sign-in", {'email': '', 'password': '1234'})
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['email'][0]['code'] == 'blank'

    def test_not_allow_invalid_email(self, anonymous_api):
        response = anonymous_api.post("/api/users/sign-in", {'email': '$invalid$', 'password': '1234'})
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['email'][0]['code'] == 'invalid'

    def test_not_allow_empty_password(self, anonymous_api):
        response = anonymous_api.post("/api/users/sign-in", {'email': 'foo', 'password': ''})
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['password'][0]['code'] == 'blank'

    def test_complain_if_non_existing_email(self, anonymous_api):
        response = anonymous_api.post("/api/users/sign-in", {'email': 'some@non-existing.com', 'password': '1234'})
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['email'][0]['code'] == 'user_not_found'

    def test_complain_if_incorrect_password(self, anonymous_api, user):
        response = anonymous_api.post("/api/users/sign-in", {'email': user.email, 'password': '4321'})
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['password'][0]['code'] == 'wrong_password'

    def test_not_allow_inactive_user(self, anonymous_api, user):
        user.is_active = False
        user.save(update_fields=('is_active',))

        response = anonymous_api.post("/api/users/sign-in", {'email': user.email, 'password': 'abc123'})
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['email'][0]['code'] == 'user_deactivated'
