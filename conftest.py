from decimal import Decimal

import pytest
from django.contrib.auth.models import User
from rest_framework.test import APIClient as OriginalAPIClient

from apps.shape.enums.shape_type import ShapeType
from apps.shape.models import Shape


@pytest.fixture
def APIClient(settings):
    def _APIClient(domain, *args, **kwargs):
        settings.ALLOWED_HOSTS = [*settings.ALLOWED_HOSTS, domain]
        return OriginalAPIClient(SERVER_NAME=domain, HTTP_USER_AGENT="api-test/0.0", *args, **kwargs)

    return _APIClient


@pytest.fixture
def anonymous_api(APIClient):
    """ Used when Domain does not matter """
    return APIClient('localhost')


# Signed In


@pytest.fixture
def user(user_password):
    return User.objects.create_user("user@example.com", "user@example.com", user_password)


@pytest.fixture
def user_password():
    return "abc123"


@pytest.fixture
def user_api(APIClient, user):
    api = APIClient("user.localhost")
    api.force_login(user)
    return api


@pytest.fixture
def other_user(other_user_password):
    return User.objects.create_user("other-user@example.com", "other-user@example.com", user_password)


@pytest.fixture
def other_user_password():
    return "efg456"


@pytest.fixture
def other_user_api(APIClient, user):
    api = APIClient("user.localhost")
    api.force_login(user)
    return api


# Shape Record


@pytest.fixture
def shape():
    return Shape.objects.create(
        shape_type=ShapeType.Rectangle,
        base=Decimal('1'),
        height=Decimal('1'),
        area=Decimal('1'),
        perimeter=Decimal('1'),
    )
