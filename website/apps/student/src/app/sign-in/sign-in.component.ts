import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@shape-season/document/services';
import { ErrorDetailMap, errorDetailToCodeMap } from '@shape-season/document/shapes';
import { RouteName } from '@shape-season/shared/enums';

@Component({
  selector: 'ss-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent {
  constructor(
    private readonly fb: FormBuilder,
    private readonly user: UserService,
    private readonly router: Router,
  ) {
  }

  public readonly RouteName = RouteName;
  public readonly form = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  submit() {
    this.user.signIn(this.form.value)
      .then(() => this.router.navigate(['/', RouteName.shapes]))
      .catch((error: HttpErrorResponse) => {
        const validationError = error.error as ErrorDetailMap;

        this.form.get('email').setErrors(errorDetailToCodeMap(validationError.email));
        this.form.get('password').setErrors(errorDetailToCodeMap(validationError.password));
      });
  }
}
