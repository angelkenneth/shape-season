from django.conf.urls import include, url
from rest_framework.routers import SimpleRouter

from .viewsets import ShapeViewSet

routes = SimpleRouter(trailing_slash=False)
routes.register("shapes", ShapeViewSet)

urlpatterns = [
    url(r"", include(routes.urls)),
]
