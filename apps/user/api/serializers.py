from django.contrib.auth.models import User
from rest_framework import serializers, fields
from rest_framework.validators import UniqueValidator

from libs.validation_errors.field_error import field_error


class UserCreateSerializer(serializers.ModelSerializer):
    first_name = fields.CharField(required=True)
    last_name = fields.CharField(required=True)
    email = fields.CharField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])
    password = fields.CharField(required=True)

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password',
        )

    def to_internal_value(self, data):
        validated_data = super().to_internal_value(data)
        email = validated_data['email'].lower()
        return {
            **validated_data,
            'email': email,
            'username': email,
        }

    def to_representation(self, instance):
        return UserReadSerializer(instance).data

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = super().create(validated_data)
        user.set_password(password)
        user.save(update_fields=('password',))
        return user


class UserReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'pk',
            'id',
            'first_name',
            'last_name',
            'email',
            'is_staff',
            'is_active',
            'date_joined',
        )


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'is_active',
        )

    def to_representation(self, instance):
        return UserReadSerializer(instance).data


class UserSignInSerializer(serializers.Serializer):
    email = fields.EmailField(required=True)
    password = fields.CharField(required=True)

    def to_internal_value(self, data):
        # noinspection PyAttributeOutsideInit
        self._validated_data = validated_data = super().to_internal_value(data)

        password = validated_data['password']

        user = self.get_user(validated_data)
        self.do_validate_user(user)
        self.do_validate_password(user, password)

        self.instance = user

        return {}

    def get_user(self, validated_data):
        try:
            return User.objects.get(email__iexact=validated_data['email'])
        except User.DoesNotExist:
            raise field_error('email', 'user_not_found')

    def do_validate_user(self, user):
        if not user.is_active:
            raise field_error('email', 'user_deactivated')

    def do_validate_password(self, user, password):
        if not user.check_password(password):
            raise field_error('password', 'wrong_password')

    def to_representation(self, instance):
        return UserReadSerializer(instance).data
