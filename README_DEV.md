# ShapeSeason

Shape CRUD.

## Development

### Generating Script

```bash
docker run -it -v $PWD:/init -w /init python:3 bash \
    && adduser human --system --shell /bin/bash --group --disabled-password --uid 1000 \
    && pip install django \
    && su human

mkdir shape-season \
    && cd shape-season \
    && django-admin startproject ShapeSeason \
    && echo "Done."
```
