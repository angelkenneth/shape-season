import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserShape } from '@shape-season/document/shapes';
import { mapTo } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private readonly http: HttpClient,
  ) {
  }

  signIn(credentials: UserShape.SignInArguments): Promise<UserShape.Document> {
    return this.http.post('/api/users/sign-in', credentials)
      .toPromise()
      .then(r => r as UserShape.Document);
  }

  register(userData: UserShape.RegistrationArguments): Promise<UserShape.Document> {
    return this.http.post('/api/users', userData)
      .toPromise()
      .then(r => r as UserShape.Document);
  }

  signOut(): Promise<void> {
    return this.http.delete('/api/users/sign-out')
      .pipe(mapTo(void 0))
      .toPromise();
  }
}
