/**
 * Only for Client Website routes, not Functions routes
 */
export enum RouteName {
  emptyRoot = '',
  register = 'register',
  shapes = 'shapes',
  signIn = 'sign-in',
}
