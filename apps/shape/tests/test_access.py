import pytest
from rest_framework.status import HTTP_403_FORBIDDEN


@pytest.mark.django_db
class TestAuthentication:
    def test_GET_anonymous_access(self, anonymous_api):
        response = anonymous_api.get('/api/shapes')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_POST_anonymous_access(self, anonymous_api):
        response = anonymous_api.post('/api/shapes')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_PATCH_anonymous_access(self, anonymous_api, shape):
        response = anonymous_api.patch(f'/api/shapes/{shape.id}')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_DELETE_anonymous_access(self, anonymous_api, shape):
        response = anonymous_api.delete(f'/api/shapes/{shape.id}')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_GET_human_access(self, user_api):
        response = user_api.get('/api/shapes')
        assert response.status_code != HTTP_403_FORBIDDEN

    def test_POST_human_access(self, user_api):
        response = user_api.post('/api/shapes')
        assert response.status_code != HTTP_403_FORBIDDEN

    def test_PATCH_human_access(self, user_api, shape):
        response = user_api.patch(f"/api/shapes/{shape.id}")
        assert response.status_code != HTTP_403_FORBIDDEN

    def test_DELETE_human_access(self, user_api, shape):
        response = user_api.delete(f'/api/shapes/{shape.id}')
        assert response.status_code != HTTP_403_FORBIDDEN
