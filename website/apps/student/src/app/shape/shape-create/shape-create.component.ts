import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroupDirective, Validators } from '@angular/forms';
import { ShapeService } from '@shape-season/document/services';
import { ErrorDetailMap, errorDetailToCodeMap, ShapeShape } from '@shape-season/document/shapes';
import { BehaviorSubject } from 'rxjs';
import { REFRESH_LIST } from '../shape.tokens';

@Component({
  selector: 'ss-shape-create',
  templateUrl: './shape-create.component.html',
  styleUrls: ['./shape-create.component.scss'],
})
export class ShapeCreateComponent {
  constructor(
    private readonly shape: ShapeService,
    private readonly fb: FormBuilder,
    @Inject(REFRESH_LIST) private readonly refreshList: BehaviorSubject<void>,
  ) {
  }

  public isSubmitting = false;
  public readonly ShapeTypeTuples = ShapeShape.ShapeTypeTuples;
  public readonly form = this.fb.group({
    shape_type: ['', Validators.required],
    base: ['', Validators.required],
    height: ['', Validators.required],
  });

  async submit(formGroup: FormGroupDirective) {
    this.isSubmitting = true;
    this.shape.create(this.form.value)
      .then(() => {
        this.form.reset();
        formGroup.resetForm();
        this.refreshList.next();
      })
      .catch((error: HttpErrorResponse) => {
        const validationError = error.error as ErrorDetailMap;

        this.form.get('shape_type').setErrors(errorDetailToCodeMap(validationError.shape_type));
        this.form.get('base').setErrors(errorDetailToCodeMap(validationError.base));
        this.form.get('height').setErrors(errorDetailToCodeMap(validationError.height));
      });
    this.isSubmitting = false;
  }
}
