export namespace ShapeShape {
  export interface Document {
    pk: string;
    id: string;
    created: string;
    shape_type: string;
    base: string;
    height: string;
    area: string;
    perimeter: string;
  }

  export interface CreateArgument {
    shape_type: string;
    base: string;
    height: string;
  }

  export interface UpdateArgument {
    base: string;
    height: string;
  }

  export enum ShapeType {
    Triangle = 'triangle',
    Rectangle = 'rectangle',
    Square = 'square',
    Diamond = 'diamond',
  }

  export enum ShapeTypeDisplay {
    Triangle = 'Triangle',
    Rectangle = 'Rectangle',
    Square = 'Square',
    Diamond = 'Diamond',
  }

  export const ShapeTypeDisplayMap: Record<ShapeType, ShapeTypeDisplay> = {
    [ShapeType.Triangle]: ShapeTypeDisplay.Triangle,
    [ShapeType.Rectangle]: ShapeTypeDisplay.Rectangle,
    [ShapeType.Square]: ShapeTypeDisplay.Square,
    [ShapeType.Diamond]: ShapeTypeDisplay.Diamond,
  };

  export interface ShapeTuple {
    value: ShapeType;
    display: ShapeTypeDisplay;
  }

  export const ShapeTypeTuples: ShapeTuple[] = Object.entries(ShapeTypeDisplayMap)
    .map(([name, display]) => ({ value: name as ShapeType, display }));
}
