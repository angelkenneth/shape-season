import { Component, Inject, Input, OnInit } from '@angular/core';
import { ShapeService } from '@shape-season/document/services';
import { ShapeShape } from '@shape-season/document/shapes';
import { BehaviorSubject } from 'rxjs';
import { REFRESH_LIST } from '../shape.tokens';

@Component({
  selector: 'ss-shape-item',
  templateUrl: './shape-item.component.html',
  styleUrls: ['./shape-item.component.scss'],
})
export class ShapeItemComponent {
  constructor(
    private readonly shape$: ShapeService,
    @Inject(REFRESH_LIST) private readonly refreshList: BehaviorSubject<void>,
  ) {
  }

  public updateMode = false;
  public readonly ShapeTypeDisplayMap = ShapeShape.ShapeTypeDisplayMap;
  @Input() public shape: ShapeShape.Document;
  @Input() public ordinal: number;

  toUpdateMode() {
    this.updateMode = true;
  }

  async delete() {
    if (confirm('Delete shape?')) {
      await this.shape$.delete(this.shape.id);
      this.refreshList.next();
    }
  }
}
