import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteName } from '@shape-season/shared/enums';
import { RegisterComponent } from './register.component';


const routes: Routes = [
  { path: RouteName.emptyRoot, component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterRoutingModule {
}
