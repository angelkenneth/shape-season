import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteName } from '@shape-season/shared/enums';


const routes: Routes = [
  { path: RouteName.shapes, loadChildren: () => import('./shape/shape.module').then(m => m.ShapeModule) },
  { path: RouteName.signIn, loadChildren: () => import('./sign-in/sign-in.module').then(m => m.SignInModule) },
  { path: RouteName.register, loadChildren: () => import('./register/register.module').then(m => m.RegisterModule) },
  { path: '**', redirectTo: RouteName.signIn },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
