from decimal import Decimal

import pytest
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK

from apps.shape.enums.shape_type import ShapeType
from apps.shape.models import Shape


@pytest.mark.django_db
class TestCalculatedField:
    def test_POST_base_and_height(self, user_api):
        response = user_api.post('/api/shapes', {
            'shape_type': ShapeType.Rectangle,
            'base': Decimal(3),
            'height': Decimal(3),
        })
        assert response.status_code == HTTP_201_CREATED

        shape = Shape.objects.get(pk=response.data['pk'])
        assert shape.area == Decimal(9)
        assert shape.perimeter == Decimal(12)

    def test_update_fields(self, user_api, shape):
        response = user_api.patch(f'/api/shapes/{shape.id}', {
            'shape_type': ShapeType.Rectangle,
            'base': Decimal(3),
            'height': Decimal(3),
        })
        assert response.status_code == HTTP_200_OK

        shape.refresh_from_db()
        assert shape.area == Decimal(9)
        assert shape.perimeter == Decimal(12)

    def test_partial_update(self, user_api, shape):
        shape.shape_type = ShapeType.Rectangle
        shape.height = Decimal(3)
        shape.save(update_fields=('shape_type', 'height'))

        response = user_api.patch(f'/api/shapes/{shape.id}', {'base': Decimal(3), })
        assert response.status_code == HTTP_200_OK

        shape.refresh_from_db()
        assert shape.area == Decimal(9)
        assert shape.perimeter == Decimal(12)
