from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from apps.shape.api.serializers import ShapeSerializer
from apps.shape.models import Shape


class ShapeViewSet(viewsets.ModelViewSet):
    queryset = Shape.objects.all().order_by('-created')
    serializer_class = ShapeSerializer

    @action(detail=False, methods=['POST'], url_path='ping', permission_classes=[AllowAny])
    def ping(self, request):
        return Response({'hello': 'world'})
