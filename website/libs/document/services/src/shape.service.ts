import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ShapeShape } from '@shape-season/document/shapes';
import { mapTo } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ShapeService {
  constructor(
    private readonly http: HttpClient,
  ) {
  }

  list(): Promise<ShapeShape.Document[]> {
    return this.http.get('/api/shapes')
      .toPromise()
      .then(r => r as ShapeShape.Document[]);
  }

  create(value: ShapeShape.CreateArgument): Promise<ShapeShape.Document> {
    return this.http.post('/api/shapes', value)
      .toPromise()
      .then(r => r as ShapeShape.Document);
  }

  delete(shapeId: string): Promise<void> {
    return this.http.delete(`/api/shapes/${shapeId}`)
      .pipe(mapTo(void 0))
      .toPromise();
  }

  update(shapeId: string, partialData: ShapeShape.UpdateArgument): Promise<ShapeShape.Document> {
    return this.http.patch(`/api/shapes/${shapeId}`, partialData)
      .toPromise()
      .then(r => r as ShapeShape.Document);
  }
}
