import { InjectionToken } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export const REFRESH_LIST = new InjectionToken<BehaviorSubject<null>>('Changing the default value');
