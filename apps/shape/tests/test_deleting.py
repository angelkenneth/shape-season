import pytest
from rest_framework.status import HTTP_204_NO_CONTENT

from apps.shape.models import Shape


@pytest.mark.django_db
class TestDeletion:
    def test_DELETE_human_access(self, user_api, shape):
        response = user_api.delete(f'/api/shapes/{shape.id}')
        assert response.status_code == HTTP_204_NO_CONTENT
        with pytest.raises(Shape.DoesNotExist):
            assert Shape.objects.get(pk=shape.id)
