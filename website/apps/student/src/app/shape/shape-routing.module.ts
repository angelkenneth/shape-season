import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteName } from '@shape-season/shared/enums';
import { ShapeComponent } from './shape.component';


const routes: Routes = [
  { path: RouteName.emptyRoot, component: ShapeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShapeRoutingModule {
}
