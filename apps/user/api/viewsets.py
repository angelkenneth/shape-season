from django.contrib.auth import logout, login
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT

from apps.user.api import serializers
from libs.permissions.user import UserRecordPermission


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserReadSerializer
    permission_classes = (UserRecordPermission,)
    queryset = User.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return serializers.UserCreateSerializer
        if self.request.method == 'PATCH':
            return serializers.UserUpdateSerializer

        return super().get_serializer_class()

    def perform_create(self, serializer):
        super().perform_create(serializer)
        login(self.request, serializer.instance)

    @action(detail=False, methods=['POST'], url_path='sign-in', permission_classes=(AllowAny,))
    def sign_in(self, request):
        serial = serializers.UserSignInSerializer(data=request.data, context=self.get_serializer_context())
        serial.is_valid(True)

        logout(request)
        login(request, serial.instance)

        return Response(serial.data)

    @action(detail=False, methods=['DELETE'], url_path='sign-out', permission_classes=(AllowAny,))
    def sign_out(self, request):
        logout(request)
        return Response(status=HTTP_204_NO_CONTENT)
