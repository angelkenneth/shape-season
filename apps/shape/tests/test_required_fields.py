import pytest
from rest_framework.status import HTTP_400_BAD_REQUEST


@pytest.mark.django_db
class TestRequiredFields:
    def test_area_and_perimeter(self, user_api):
        response = user_api.post('/api/shapes')
        assert response.status_code == HTTP_400_BAD_REQUEST
        assert response.data['shape_type'][0]['code'] == 'required'
        assert response.data['base'][0]['code'] == 'required'
        assert response.data['height'][0]['code'] == 'required'
