from uuid import uuid4

from django.db import models

from apps.shape.enums.shape_type import ShapeTypeTuples, ShapeTypeMaxLength


class Shape(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    shape_type = models.CharField(choices=ShapeTypeTuples, max_length=ShapeTypeMaxLength)
    base = models.DecimalField(max_digits=40, decimal_places=6)
    height = models.DecimalField(max_digits=40, decimal_places=6)
    area = models.DecimalField(max_digits=40, decimal_places=6)
    perimeter = models.DecimalField(max_digits=40, decimal_places=6)
