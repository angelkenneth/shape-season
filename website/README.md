# ShapeSeason 

Shape CRUD.

## Development

### Generating Script

```bash
docker run -it -v $PWD:/init -w /init node:10.15.3 su node

NG_CLI_ANALYTICS=true \
    npx create-nx-workspace shape-season --preset=empty --cli=angular --style=scss \
    && cd shape-season \
    && npx ng add @nrwl/angular --defaults \
    && npx ng g @nrwl/angular:application student --directory --routing=true --prefix=ss --style=scss --skip-tests \
    && npx ng add @angular/material --defaults \
    && npx ng generate @nrwl/workspace:library document/services --directory \
    && npx ng generate @nrwl/workspace:library document/shapes --directory \
    && npx ng generate @nrwl/workspace:library shared/enums --directory \
    && echo "Done."
```
