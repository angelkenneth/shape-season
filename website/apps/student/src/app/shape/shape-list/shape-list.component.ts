import { Component, Inject, OnDestroy } from '@angular/core';
import { ShapeService } from '@shape-season/document/services';
import { ShapeShape } from '@shape-season/document/shapes';
import { BehaviorSubject } from 'rxjs';
import { REFRESH_LIST } from '../shape.tokens';

@Component({
  selector: 'ss-shape-list',
  templateUrl: './shape-list.component.html',
  styleUrls: ['./shape-list.component.scss'],
})
export class ShapeListComponent implements OnDestroy {
  constructor(
    private readonly shape: ShapeService,
    @Inject(REFRESH_LIST) private readonly refreshList: BehaviorSubject<void>,
  ) {
  }

  public readonly refreshListSub = this.refreshList.subscribe(() => this.refresh());
  public shapes: ShapeShape.Document[] = [];

  async refresh() {
    this.shapes = await this.shape.list();
  }

  ngOnDestroy() {
    this.refreshListSub.unsubscribe();
  }
}
