from django.conf.urls import include, url
from rest_framework.routers import SimpleRouter

from .viewsets import UserViewSet

routes = SimpleRouter(trailing_slash=False)
routes.register("users", UserViewSet)

urlpatterns = [
    url(r"", include(routes.urls)),
]
