import pytest
from rest_framework.status import HTTP_403_FORBIDDEN


@pytest.mark.django_db
class TestUserAuthentication:
    def test_GET_list_anonymous_access(self, anonymous_api):
        response = anonymous_api.get('/api/users')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_GET_detail_anonymous_access(self, anonymous_api, user):
        response = anonymous_api.get(f'/api/users/{user.id}')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_POST_anonymous_access(self, anonymous_api):
        response = anonymous_api.post('/api/users')
        assert response.status_code != HTTP_403_FORBIDDEN

    def test_PATCH_anonymous_access(self, anonymous_api, user):
        response = anonymous_api.patch(f'/api/users/{user.id}')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_DELETE_anonymous_access(self, anonymous_api, user):
        response = anonymous_api.delete(f'/api/users/{user.id}')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_GET_list_user_access(self, user_api):
        response = user_api.get('/api/users')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_GET_detail_own_user_record_access(self, user_api, user):
        response = user_api.get(f'/api/users/{user.id}')
        assert response.status_code != HTTP_403_FORBIDDEN

    def test_GET_detail_other_user_record_access(self, user_api, other_user):
        response = user_api.get(f'/api/users/{other_user.id}')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_POST_user_access(self, user_api):
        response = user_api.post('/api/users')
        assert response.status_code == HTTP_403_FORBIDDEN

    def test_PATCH_user_access(self, user_api, user):
        response = user_api.patch(f"/api/users/{user.id}")
        assert response.status_code != HTTP_403_FORBIDDEN

    def test_DELETE_user_access(self, user_api, user):
        response = user_api.delete(f'/api/users/{user.id}')
        assert response.status_code != HTTP_403_FORBIDDEN
