export namespace UserShape {
  export interface Document {
    pk: string;
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    is_staff: boolean;
    is_active: boolean;
    date_joined: string;
  }

  export interface SignInArguments {
    email: string;
    password: string;
  }

  export interface RegistrationArguments {
    email: string;
    first_name: string;
    last_name: string;
    password: string;
  }
}
