import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ShapeService } from '@shape-season/document/services';
import { ErrorDetailMap, errorDetailToCodeMap, ShapeShape } from '@shape-season/document/shapes';
import { BehaviorSubject } from 'rxjs';
import { ShapeItemComponent } from '../shape-item/shape-item.component';
import { REFRESH_LIST } from '../shape.tokens';

@Component({
  selector: 'ss-shape-update',
  templateUrl: './shape-update.component.html',
  styleUrls: ['./shape-update.component.scss'],
})
export class ShapeUpdateComponent implements OnInit {
  constructor(
    private readonly shape$: ShapeService,
    private readonly fb: FormBuilder,
    private readonly patent: ShapeItemComponent,
    @Inject(REFRESH_LIST) private readonly refreshList: BehaviorSubject<void>,
  ) {
  }

  public readonly ShapeTypeDisplayMap = ShapeShape.ShapeTypeDisplayMap;
  @Input() public shape: ShapeShape.Document;
  @Input() public ordinal: number;
  public readonly form = this.fb.group({
    base: ['', Validators.required],
    height: ['', Validators.required],
  });

  ngOnInit() {
    this.form.patchValue(this.shape);
  }

  submit() {
    this.shape$.update(this.shape.id, this.form.value)
      .then(() => {
        this.refreshList.next();
        this.patent.updateMode = false;
      })
      .catch((error: HttpErrorResponse) => {
        const validationError = error.error as ErrorDetailMap;

        this.form.get('base').setErrors(errorDetailToCodeMap(validationError.base));
        this.form.get('height').setErrors(errorDetailToCodeMap(validationError.height));
      });
  }

  cancel() {
    this.patent.updateMode = false;
  }
}
