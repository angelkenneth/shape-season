from rest_framework import permissions


class UserRecordPermission(permissions.BasePermission):
    def has_permission(self, request, viewset):
        if viewset.detail:
            if request.user.is_anonymous:
                return False
            return True

        else:
            if request.method == 'POST' and request.user.is_anonymous:
                return True

        return False

    def has_object_permission(self, request, view, user_record):
        if request.user.pk == user_record.pk:
            return True

        return False
