from operator import itemgetter


class ShapeType:
    Triangle = 'triangle'
    Rectangle = 'rectangle'
    Square = 'square'
    Diamond = 'diamond'


class ShapeTypeDisplay:
    Triangle = 'Triangle'
    Rectangle = 'Rectangle'
    Square = 'Square'
    Diamond = 'Diamond'


ShapeTypeDisplayMap = {
    ShapeType.Triangle: ShapeTypeDisplay.Triangle,
    ShapeType.Rectangle: ShapeTypeDisplay.Rectangle,
    ShapeType.Square: ShapeTypeDisplay.Square,
    ShapeType.Diamond: ShapeTypeDisplay.Diamond,
}

ShapeTypeTuples = list(map(lambda t: (t[0], t[1]), ShapeTypeDisplayMap.items()))
ShapeTypeMaxLength = max(map(len, map(itemgetter(0), ShapeTypeTuples)))
